from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from secondHand.models import *
from .forms import *
from .tasks import *
from notification.models import Notification

def get_customer(user):
    profile = Profile.objects.get(user=user)
    return Customer.objects.get(profile=profile)



def charge_account(request):
    if request.method == 'GET':
        user = request.user
        credit = get_customer(user).credit
        return render(request, 'charge_account.html', {'credit':credit})
    if request.method == 'POST':
        form = ChargeForm(request.POST)
        if form.is_valid():
            val = form.cleaned_data['value']
            if val < 0:
                log = 'قیمت وارد شده معتبر نیست'
                return render(request, 'sent_to_bank.html', {'trans_log': log})
            user = request.user
            customer = get_customer(user)
            transaction = Transaction(value=val, customer=customer)
            transaction.save()
            fail_transaction.apply_async((transaction.pk,),countdown=settings.PAYMENT_VALID_TIME)
            log = "پردازش تراکنش شما به درگاه پرداخت منتقل شد"
            current_site = get_current_site(request)
            domain = current_site.domain
            return render(request, 'sent_to_bank.html', {'trans_log': log, 'trans_pk':transaction.pk, 'domain':domain})
        else:
            log = 'قیمت وارد شده معتبر نیست'
            return render(request, 'sent_to_bank.html', {'trans_log': log})


def transaction_result(request, trans_id, result):
    try:
        transacion = Transaction.objects.get(pk=int(trans_id))
        if transacion.status == "pending":
            if result == "failed":
                transacion.fail()
                log = "فرایند پرداخت با مشکل مواجه شد.لطفا مجدد برای پرداخت اقدام کنید."
                return render(request, 'sent_to_bank.html', {'trans_log': log})
            elif result == "done":
                transacion.succeed()
                log = "تراکنش شماره {0} با موفقیت تایید شد".format(transacion.pk)
                return render(request, 'sent_to_bank.html', {'trans_log': log})
            else:
                log = "آدرس نامعتبر می‌باشد."
                return render(request, 'sent_to_bank.html', {'trans_log': log})

        elif transacion.status == "succeed":
            log = "تراکنش پیش از این پردازش شده است"
            return render(request, 'sent_to_bank.html', {'trans_log': log})
        elif transacion.status == "failed":
            log = "تراکنش مورد نظر دیگر معتبر نمی‌باشد. لطفا مجدد برای پرداخت اقدام کنید."
            return render(request, 'sent_to_bank.html', {'trans_log': log})
    except Exception:
        log = "آدرس نامعتبر می‌باشد."
        return render(request, 'sent_to_bank.html', {'trans_log': log})