from django.db import models
from django.utils import timezone
from secondHand.models import Customer
from notification.models import Notification


# Create your models here.

class Transaction(models.Model):
    value = models.IntegerField(blank=False, null=False)
    time = models.DateTimeField(default=timezone.now(), blank=False, null=False)
    customer = models.ForeignKey(Customer, blank=False, null=False, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, default="pending")

    def succeed(self):
        self.status = "succeed"
        customer = self.customer
        customer.add_credit(self.value)
        notif = Notification(customer=customer,message="تراکنش شما با موفقیت تایید شد و اعتبار شما به میزان {0} واحد افزایش گردید".format(str(self.value)))
        notif.save()
        self.save()

    def show_status(self):
        if self.status == "pending":
            return "در حال انتظار"
        if self.status == "succeed":
            return "موفق"
        if self.status == "failed":
            return "ناموفق"
        else:
            return "نامعلوم"

    def fail(self):
        self.status = "failed"
        self.save()
        notif = Notification(customer=self.customer, message="تراکنش اخیر شما ناموفق به پایان رسید")
        notif.save()
