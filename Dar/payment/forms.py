from django import forms
from .models import *

class ChargeForm(forms.ModelForm):
    class Meta:
        model = Transaction
        exclude = ['time', 'customer', 'status']