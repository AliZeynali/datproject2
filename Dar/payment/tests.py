from django.test import TestCase
from django.utils import timezone
from .models import *
from secondHand.models import *
from .tasks import *
from django.contrib.auth.models import User


class Succeed_Transaction(TestCase):
    def setUp(self):
        print("Setting Up")
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')

    def successful_transaction(self):
        print("Testing succeeded transaction")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        self.assertEqual(customer.credit, 0)
        transaction = Transaction(customer=customer, value=10)
        self.assertEqual(transaction.status, 'pending')
        transaction.succeed()
        self.assertEqual(transaction.status, 'succeed')
        self.assertEqual(customer.credit, 10)
