from celery import shared_task
from django.conf import settings
from .models import Transaction
from Auction.models import Auction


@shared_task
def fail_transaction(transaction_id):
    try:
        print("TRANSACTION_ID is\n")
        print(transaction_id)
        transaction = Transaction.objects.get(pk=transaction_id)
        if transaction.status == "pending":
            transaction.fail()
    except Exception:
        print("CELERY ERROR:\t\tTransaction Error")
        pass


@shared_task
def finish_auction(auction_pk):
    try:
        print("AUCTION ID is\n")
        print(auction_pk)
        auction = Auction.objects.get(pk=auction_pk)
        if auction.status == "open":
            auction.close()
    except Exception:
        print("CELERY ERROR\t\tAuction Error")
        pass
