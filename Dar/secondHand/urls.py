from django.urls import path, re_path
from django.conf.urls import url, include
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from register.views import *
from ContactUs.views import *
from customer.views import *
from django.contrib.auth import views as auth_views
from payment.views import *

urlpatterns = [
                  url(r'^customer/', include(('customer.urls', 'customer'), namespace="customer")),
                  path('', home, name='home'),
                  path('index', index, name='index'),
                  path('login', login, name='login'),
                  path('logout/', logout, name='logout'),
                  path('all_ads', all_ads, name='all_ads'),
                  path('recent_adv', recent_adv_menu, name='recent_adv'),
                  path('search_by_category', search_by_category, name='search_by_category'),
                  path('search_by_address', search_by_address, name='search_by_address'),
                  path('contact_us', contact, name='contact'),
                  path('complaint', complaint, name='complaint'),
                  path('search', search, name='search'),
                  path('register', register, name='register'),
                  url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                      activate, name='activate'),
                  url(r'^(?P<ad_id>[0-9]+)/$', view_ad_raw, name='view_ad_raw'),
                  url(r'^(?P<ad_id>[0-9]+)/add_comment/$', add_comment, name='add_comment'),
                  path('password_reset/', auth_views.PasswordResetView.as_view(),
                       {'template_name': 'registration/password_reset_form.html'}, name='password_reset'),
                  path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(),
                       {'template_name': 'registration/password_reset_done.html'}, name='password_reset_done'),
                  re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
                          auth_views.PasswordResetConfirmView.as_view(),
                          {'template_name': 'registration/password_reset_confirm.html'}, name='password_reset_confirm'),
                  path('reset/done/', auth_views.PasswordResetCompleteView.as_view(),
                       {'template_name': 'registration/password_reset_confirm.html'}, name='password_reset_complete'),
                  url(r'^transaction_result/(?P<trans_id>[\w|\W]+)/(?P<result>[\w|\W]+)/$',
                      transaction_result, name='transaction_result'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
