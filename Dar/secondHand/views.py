from .forms import LoginForm
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.decorators import login_required
from .forms import LoginForm
from .models import *
from django.http import HttpResponseRedirect


def homePage(request):
    return render(request, 'Home.html', {'modalDisplay': 1})


def index(request):
    if request.user.is_authenticated:
        profiles = Profile.objects.filter(user=request.user)
        if profiles.exists():
            profile = profiles[0]
            if profile.role == 'customer':
                # return render(request, 'costumerBase.html')
                return HttpResponseRedirect("/customer/home/")
        return redirect("home")
    return redirect('login')


def logout(request):
    if request.user.is_authenticated:
        auth_logout(request)
    return redirect('index')


def loginpage(request):
    return render(request, 'costumerBase.html')


def login(request):
    if request.user.is_authenticated:
        try:
            profile = Profile.objects.get(user=request.user)
        except Exception:
            profile = None
        if profile != None and profile.role == 'customer':
            return render(request, 'costumerBase.html')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                auth_login(request, user)
                return redirect('index')
            return render(request, 'login.html', {'log': "کاربری با یوزر و پسورد وارد شده موجود نمی‌باشد"})
    if request.method == 'GET':
        return render(request, 'login.html')


def recent_adv_menu(request):
    number_of_item_to_show = 10
    advertises = Advertise.objects.order_by('published_date')[:number_of_item_to_show]
    return render(request, 'advertise_show.html', {'advertises': advertises})


def search_by_category(request):
    number_of_item_to_show = 10
    cat = request.POST.get('category', None)
    advertises = Advertise.objects.filter(category=cat).order_by('published_date')[:number_of_item_to_show]
    return render(request, 'advertise_show.html', {'advertises': advertises})


def search_by_address(request):
    number_of_item_to_show = 10
    add = request.POST.get('address', None)
    advertises = Advertise.objects.filter(address=add).order_by('published_date')[:number_of_item_to_show]
    return render(request, 'advertise_show.html', {'advertises': advertises})


def home(request):
    return render(request, "Home.html")


def search(request):
    return render(request, "search.html")


def all_ads(request):
    advertises = Advertise.objects.order_by('published_date')
    return render(request, 'all_ads.html', {'advertises': advertises})

def view_ad_raw(request, ad_id):
    advertise = Advertise.objects.get(pk=ad_id)
    loc = Location_Advertise.objects.get(advertise=advertise)
    if request.method == 'POST':
        advertise.status = 'Has been sold'
        advertise.save()
    images = Image_Advertise.objects.filter(advertise=advertise)
    for im in images:
        urls = "Dar/"
    comments = Comment.objects.filter(advertise=advertise)
    urls = []
    for im in images:
        print(im.image.url)
    return render(request, 'view_ad_raw.html',
                  {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat})


@login_required
def add_comment(request, ad_id):
    print("comment")
    advertise = Advertise.objects.get(pk=ad_id)
    images = Image_Advertise.objects.filter(advertise=advertise)
    comments = Comment.objects.filter(advertise=advertise)
    loc = Location_Advertise.objects.get(advertise=advertise)
    user = request.user
    user_form = SignupForm(instance=user)
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.advertise = advertise
                profile = Profile.objects.get(user=request.user)
                sender = Customer.objects.get(profile=profile)
                comment.sender = sender
                comment.time = datetime.now().date()
                comment.save()
    else:
        log = "تنها کاربران عضو سایت قادر به ثبت نظر هستند.\n برای ثبت نظر باید ابتدا وارد سایت شوید."
        return render(request, 'view_ad_raw.html',
                      {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat, 'modalDisplay': 1, 'log':log})
    return render(request, 'view_ad_raw.html',
                  {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat})

