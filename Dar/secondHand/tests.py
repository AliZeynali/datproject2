from django.test import TestCase
from django.utils import timezone

from .models import *


class CategoryTest(TestCase):
    def setUp(self):
        cat1 = Category_type_1.objects.create(name="catTest1")
        cat2 = Category_type_2.objects.create(cat_lev1=cat1, name="catTest2")
        cat3 = Category_type_3.objects.create(cat_lev2=cat2, name="catTest3")

    def test_three_level_cat(self):
        print("Testing Category")
        cat1 = Category_type_1.objects.get(name="catTest1")
        cat2 = Category_type_2.objects.get(name="catTest2")
        cat3 = Category_type_3.objects.get(name="catTest3")
        self.assertEqual(cat1, cat2.cat_lev1)
        self.assertEqual(cat2, cat3.cat_lev2)


class CustomerTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')

    def test_creating_customer(self):
        print("Testing Customer")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        self.assertEqual(customer.phone_number, 112233)


class AdvertiseTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')
        cat1 = Category_type_1.objects.create(name="catTest1")
        cat2 = Category_type_2.objects.create(cat_lev1=cat1, name="catTest2")
        cat3 = Category_type_3.objects.create(cat_lev2=cat2, name="catTest3")
        advertise = Advertise.objects.create(title="testTitle", desc="testDesc", seller=customer, category=cat3)

    def test_creating_advertise(self):
        print("Testing Advertise")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        advertise = Advertise.objects.get(title='testTitle')
        self.assertEqual(advertise.desc, 'testDesc')
        self.assertEqual(advertise.seller, customer)

class CommentTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')
        cat1 = Category_type_1.objects.create(name="catTest1")
        cat2 = Category_type_2.objects.create(cat_lev1=cat1, name="catTest2")
        cat3 = Category_type_3.objects.create(cat_lev2=cat2, name="catTest3")
        advertise = Advertise.objects.create(title="testTitle", desc="testDesc", seller=customer, category=cat3)
        comment = Comment.objects.create(time=timezone.now(), advertise=advertise, desc="commentDesc", sender=customer)

    def test_creating_comment(self):
        print("Testing Comment")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        advertise = Advertise.objects.get(title='testTitle')
        comment = Comment.objects.get(advertise=advertise)
        self.assertEqual(comment.sender, customer)
        self.assertEqual(comment.desc, "commentDesc")
