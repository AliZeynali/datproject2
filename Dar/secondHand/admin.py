from django.contrib import admin
from .models import *
from Auction.models import *
from payment.models import *
from django.contrib.auth.models import User
from notification.models import *
from ContactUs.models import *
# Register your models here.
admin.site.register(Profile)
admin.site.register(Customer)
admin.site.register(Category_type_1)
admin.site.register(Category_type_2)
admin.site.register(Category_type_3)
admin.site.register(Advertise)
admin.site.register(Comment)
admin.site.register(Address)
admin.site.register(Transaction)
admin.site.register(Auction)
admin.site.register(Bid)
admin.site.register(Notification)
admin.site.register(Complaint)
admin.site.register(Reserve_credit)
admin.site.register(Image_Advertise)