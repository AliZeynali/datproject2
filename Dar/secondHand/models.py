from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(default='customer', max_length=20)
    nationalID = models.CharField(max_length=10, unique=True)
    location_address = models.CharField(default='', max_length=400)
    birthday = models.DateTimeField(default='2000-01-01', null=True)



class Address(models.Model):
    lat = models.DecimalField(max_digits=9, decimal_places=6, default=35.70530498029150)
    lng = models.DecimalField(max_digits=9, decimal_places=6, default=51.34547698029149)
    profile=models.ForeignKey(Profile, on_delete=models.CASCADE)


class Customer(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)
    phone_number = models.IntegerField()
    credit = models.IntegerField(default="0")
    invitation_code = models.CharField(default="0", max_length=30)

    def get_published_advertises_by_me(self):
        return Advertise.objects.get(seller=self)

    def get_bought_advertises(self):
        return Advertise.objects.get(buyer=self)

    def add_comment(self, desc, advertise):
        comment = Comment(time=timezone.now(), desc=desc, sender=self, advertise=advertise)
        comment.save()

    def add_advertise(self, title, desc, price, off, category1, category2, category3, address):
        cat1 = Category_type_1.objects.get(name=category1)
        cat2 = Category_type_2.objects.get(name=category2, cat_lev1=cat1)
        cat3 = Category_type_3.objects.get(name=category3, cat_lev2=cat2)
        advertise = Advertise(title=title, desc=desc, price=price, off= off, seller=self, category=cat3, address=address)
        advertise.save()

    def add_credit(self, val):
        self.credit += val
        self.save()

    def reserve_credit(self, value, advertise):
        reserve_cr = Reserve_credit(value=value, customer=self, advertise=advertise)
        reserve_cr.save()
        self.credit -= value
        self.save()

    def free_credit(self, reserve_cr):
        reserve_cr.status = "free"
        reserve_cr.save()
        self.credit += reserve_cr.value
        self.save()


class Category_type_1(models.Model):
    name = models.CharField(max_length=20, unique=True, primary_key=True)


class Category_type_2(models.Model):
    cat_lev1 = models.ForeignKey(Category_type_1, related_name='cat_lev1', on_delete=models.CASCADE)
    name = models.CharField(max_length=20)


class Category_type_3(models.Model):
    cat_lev2 = models.ForeignKey(Category_type_2, related_name='cat_lev2', on_delete=models.CASCADE)
    name = models.CharField(max_length=20)


class Advertise(models.Model):
    title = models.CharField(max_length=30, null=False)
    desc = models.CharField(max_length=400, null=True)
    published_date = models.DateTimeField(default=timezone.now(), null=False, blank=False)
    price = models.IntegerField(default="0")
    status = models.CharField(default='Not sold yet', max_length=20)
    accepted = models.BooleanField(default=False)
    seller = models.ForeignKey(Customer, related_name='seller', on_delete=models.CASCADE)
    buyer = models.ForeignKey(Customer, related_name='buyer', null=True, blank=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category_type_3, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    priority = models.IntegerField(default='1')
    has_auction = models.BooleanField(default=False)
    off = models.IntegerField(default=0)

    def sell_item(self, buyer):
        self.buyer = buyer
        self.status = 'has Sold'
        self.save()

    def add_image(self, image):
        image_advertise = Image_Advertise(advertise=self, image=image)
        image_advertise.save()

    def get_images(self):
        return Image_Advertise(advertise=self)


class Comment(models.Model):
    time = models.DateTimeField()
    advertise = models.ForeignKey(Advertise, on_delete=models.CASCADE, blank=False, null=False)
    desc = models.CharField(max_length=300, blank=False, null=False)
    sender = models.ForeignKey(Customer, blank=False, null=False, on_delete=models.CASCADE)



class Worker_Advertise(models.Model):
    worker = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=False, null=False)
    advertise = models.ForeignKey(Advertise, on_delete=models.CASCADE, blank=False, null=False)


class Image_Advertise(models.Model):
    image = models.ImageField(upload_to='ad_images', blank=True)
    advertise = models.ForeignKey(Advertise, on_delete=models.CASCADE, blank=False, null=False)



class Reserve_credit(models.Model):
    value = models.IntegerField()
    customer = models.ForeignKey(Customer, blank=False, null=False, on_delete=models.CASCADE)
    advertise = models.ForeignKey(Advertise, blank=False, null=False, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, default="reserved")