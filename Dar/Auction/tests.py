from django.test import TestCase
from django.utils import timezone
from secondHand.models import *
from .models import *


class AuctionTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')
        cat1 = Category_type_1.objects.create(name="catTest1")
        cat2 = Category_type_2.objects.create(cat_lev1=cat1, name="catTest2")
        cat3 = Category_type_3.objects.create(cat_lev2=cat2, name="catTest3")
        advertise = Advertise.objects.create(title="testTitle", desc="testDesc", seller=customer, category=cat3)
        auction = Auction.objects.create(item=advertise, seller=customer, deadline=timezone.now(), base_price='25')

    def test_creating_auction(self):
        print("Testing Auction")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        advertise = Advertise.objects.get(title='testTitle')
        auction = Auction.objects.get(item=advertise)
        self.assertEqual(auction.seller, customer)
        self.assertEqual(auction.base_price, 25)


class BidTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testUser')
        profile = Profile.objects.create(user=user, nationalID='110011')
        customer = Customer.objects.create(profile=profile, phone_number='112233')
        cat1 = Category_type_1.objects.create(name="catTest1")
        cat2 = Category_type_2.objects.create(cat_lev1=cat1, name="catTest2")
        cat3 = Category_type_3.objects.create(cat_lev2=cat2, name="catTest3")
        advertise = Advertise.objects.create(title="testTitle", desc="testDesc", seller=customer, category=cat3)
        auction = Auction.objects.create(item=advertise, seller=customer, deadline=timezone.now(), base_price='25')
        bid = Bid.objects.create(auction=auction, bidder=customer, date=timezone.now(), value='30')

    def test_creating_auction(self):
        print("Testing Bid")
        profile = Profile.objects.get(nationalID='110011')
        customer = Customer.objects.get(profile=profile)
        advertise = Advertise.objects.get(title='testTitle')
        auction = Auction.objects.get(item=advertise)
        bid = Bid.objects.get(auction=auction)
        self.assertEqual(bid.bidder, customer)
        self.assertEqual(bid.value, 30)
