from django.db import models
from secondHand.models import *
from django.utils import timezone


# Create your models here.
class Auction(models.Model):
    item = models.ForeignKey(Advertise, related_name='auction_item', on_delete=models.CASCADE)
    seller = models.ForeignKey(Customer, related_name='auction_seller', on_delete=models.CASCADE)
    starting_date = models.DateTimeField(default=timezone.now(), null=False, blank=False)
    deadline = models.DateTimeField(null=False, blank=False)
    base_price = models.IntegerField(default='0', null=False)
    status = models.CharField(max_length=20, default="open")

    def get_bids(self):
        bids = Bid.objects.filter(auction=self)
        return bids

    def max_bid_value(self):
        if len(Bid.objects.filter(auction=self)) == 0:
            return 0
        else:
            return Bid.objects.filter(auction=self).order_by('-value')[0].value

    def get_max_bid(self):
        if len(Bid.objects.filter(auction=self)) == 0:
            return None
        else:
            return Bid.objects.filter(auction=self).order_by('-value')[0]


    def close(self):
        self.status = "closed"
        self.save()


class Bid(models.Model):
    auction = models.ForeignKey(Auction, related_name='auction', on_delete=models.CASCADE)
    bidder = models.ForeignKey(Customer, related_name='bidder', on_delete=models.CASCADE)
    date = models.DateTimeField(null=False, blank=False)
    value = models.IntegerField(blank=False, null=False)
