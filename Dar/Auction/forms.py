from django import forms
from .models import *

class AuctionForm(forms.ModelForm):
    class Meta:
        model= Auction
        exclude=['item','seller','starting_date', 'status']


class BidForm(forms.ModelForm):
    class Meta:
        model= Bid
        exclude=['bidder','date','auction']