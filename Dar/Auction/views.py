from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.shortcuts import render
from django.shortcuts import redirect
from secondHand.models import *
from .models import *
from .forms import AuctionForm, BidForm
from django.utils.datetime_safe import datetime
from payment.tasks import *
from notification.models import Notification


def make_auction(request, ad_id):
    advertise = Advertise.objects.get(pk=ad_id)
    if request.method == 'GET':
        return render(request, 'auction.html', {'advertise': advertise})
    if request.method == 'POST':
        form = AuctionForm(request.POST)
        if form.is_valid():
            auction = form.save(commit=False)
            profile = Profile.objects.get(user=request.user)
            seller = Customer.objects.get(profile=profile)
            auction.seller = seller
            auction.item = advertise
            auction.save()
            finish_auction.apply_async((auction.pk,), eta=auction.deadline)
            return redirect('customer:make_bid', ac_id=auction.pk)
    return render(request, 'auction.html', {'advertise': advertise})


def free_last_bidder(bid):
    try:
        bidder = bid.bidder
        auction = bid.auction
        advertise = auction.item
        reserve_cr = Reserve_credit.objects.get(advertise=advertise, customer=bidder, status="reserved")
        bidder.free_credit(reserve_cr)


    except Exception as e:
        print("ERROR IN FREE_LAST_BIDDER")
        print(e)
        pass


def initiate_bid(form, request, value, auction):
    bidder = Profile.objects.get(user=request.user)
    bidder = Customer.objects.get(profile=bidder)
    credit = bidder.credit
    if (value / 10) >= credit:
        return False
    max_bid = auction.get_max_bid()
    if max_bid != None:
        free_last_bidder(max_bid)
    bid = form.save(commit=False)
    bid.bidder = bidder
    bidder.refresh_from_db()
    bid.date = datetime.now().date()
    bid.auction = auction
    bid.save()
    bidder.reserve_credit(value / 10, auction.item)
    message = "مبلغ {0} واحد به دلیل ارائه پیشنهاد در مزایده کالا با عنوان << {1} >> از حساب شما در رزرو قرار گرفت.".format(value/10,auction.item.title)
    notif = Notification(customer=bidder,message=message)
    notif.save()
    return True


def make_bid(request, ac_id):
    auction = Auction.objects.get(pk=ac_id)
    if datetime.now().date() < auction.deadline:
        if request.method == 'GET':
            return render(request, 'bid.html',
                          {'auction': auction, 'bids': auction.get_bids(), 'max_bid': auction.max_bid_value})
        if request.method == 'POST':
            form = BidForm(request.POST)
            if form.is_valid():
                value = form.cleaned_data['value']
                if value > auction.max_bid_value():
                    has_credit = initiate_bid(form, request, value, auction)
                    if not has_credit:
                        log = "موجودی شما کافی نیست. باید حداقل ۱۰ درصد مبلغ پیشنهادی خود را در موجودی کنونی داشته باشید."
                        return render(request, 'unsuccessful_bid.html', {'log': log})
                    return render(request, 'bid.html',
                                  {'auction': auction, 'bids': auction.get_bids(), 'max_bid': auction.max_bid_value})
                else:
                    log = "مبلغ پیشنهادی شما بیشتر از بهترین پیشنهاد موجود برای این مزایده نیست لکن قادر به ثبت پیشنهادتان نیستید."
                    return render(request, 'unsuccessful_bid.html', {'log': log})

        return render(request, 'bid.html',
                      {'auction': auction, 'bids': auction.get_bids(), 'max_bid': auction.max_bid_value})
    else:
        return render(request, 'end-bid.html',
                      {'auction': auction, 'bids': auction.get_bids(), 'max_bid': auction.max_bid_value})
