from django import forms
from secondHand.models import *
from Auction.models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class AdvertiseForm(forms.ModelForm):
    class Meta:
        model = Advertise
        exclude = ['published_date', 'status', 'accepted', 'seller', 'buyer' , 'priority']


class ImageForm(forms.ModelForm):
    image = forms.ImageField(label='Image')

    class Meta:
        model = Image_Advertise
        fields = ('image',)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ['time', 'advertise', 'sender']





class SearchForm(forms.Form):
    title=forms.CharField(max_length='100')
    desc=forms.CharField(max_length='1000')
    max_price=forms.IntegerField()
    min_price=forms.IntegerField()


class SignupForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length=50, required=True)
    last_name = forms.CharField(max_length=50, required=True)
    nationalID = forms.IntegerField()
    location_address = forms.CharField(max_length='400')
    phone_number = forms.IntegerField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', "password1", "password2", 'nationalID', 'location_address', 'phone_number')