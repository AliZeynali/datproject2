import json

from django.http import HttpResponse
from django.shortcuts import render
# from .forms import LoginForm, RegisterForm
from django.shortcuts import redirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from secondHand.models import *
from .forms import *
from .forms import AdvertiseForm, ImageForm, CommentForm, SignupForm
from django.utils.datetime_safe import datetime
from django.contrib.auth.decorators import login_required
from django.forms import modelformset_factory
from django.conf import settings
from datetime import timedelta
import urllib
from django.utils.encoding import smart_str
from urllib.request import urlopen
from django.db.models import Q
from payment.models import *
from notification.models import Notification

def costumerHome(request):
    number_of_item_to_show = 10
    advertises = Advertise.objects.order_by('published_date')[:number_of_item_to_show]
    return render(request, 'my_ads.html', {'advertises': advertises})


def publish_ad_form(request):
    return render(request, 'publishAd.html')


def is_valid_form(form, formset):
    if form.is_valid() and formset.is_valid():
        return True
    else:
        return False


def create_and_save_ad_form(request, form, lnglat):
    ad = form.save(commit=False)
    ad.published_date = datetime.now().date()
    profile = Profile.objects.get(user=request.user)
    seller = Customer.objects.get(profile=profile)
    ad.seller = seller
    ad.save()
    location = Address(lng=lnglat.split(':')[1], lat=lnglat.split(':')[0], profile=profile)
    location.save()
    notif = Notification(customer=seller,message="تبلیغ کالای شما با عنوان {0} در سامانه ثبت گردید".format(ad.title))
    notif.save()
    return ad


def add_image_to_ad(formset, ad):
    for form in formset.cleaned_data:
        if form:
            image = form['image']
            photo = Image_Advertise(advertise=ad, image=image)
            photo.save()


def create_lat_and_lng(request):
    lat = 35.7053049802915
    lng = 51.3454769802915
    address = request.POST['address']
    location = urllib.parse.quote_plus(smart_str(address))
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyCJAKcqOawanaYoV4SeKKKbwXRjQxYuYO8' % location
    response = urlopen(url).read()
    result = json.loads(response.decode('utf-8'))
    if result['status'] == 'OK':
        lat = str(result['results'][0]['geometry']['location']['lat'])
        lng = str(result['results'][0]['geometry']['location']['lng'])
    return lat, lng, address


@login_required
def publish_adv(request):
    ImageFormSet = modelformset_factory(Image_Advertise,
                                        form=ImageForm, extra=3)
    cat1 = Category_type_1.objects.exclude(name='others')
    cat2 = Category_type_2.objects.all()
    cat3 = Category_type_3.objects.all()
    if request.method == 'POST' and 'add' in request.POST:
        request.POST._mutable = True
        lnglat = request.POST['lnglat']
        del request.POST['lnglat']
        del request.POST['newCategory']
        request.POST._mutable = False
        form = AdvertiseForm(request.POST)
        formset = ImageFormSet(request.POST, request.FILES,
                               queryset=Image_Advertise.objects.none())
        if form.is_valid() and formset.is_valid():
            ad = create_and_save_ad_form(request, form, lnglat)
            add_image_to_ad(formset, ad)
            log = "تبلیغ شما ثبت شد"
        else:
            log = "در ارسال تبلیغ شما خطا رخ داد. لطفا مجدد تلاش کنید."

        return render(request, "costumerBase.html", {'log': log})
    elif request.method == 'POST' and 'location' in request.POST:
        lat, lng, address = create_lat_and_lng(request)
        form = AdvertiseForm()
        formset = ImageFormSet(queryset=Image_Advertise.objects.none())
        return render(request, 'publishAd.html',
                      {'form': form, 'formset': formset, 'cats1': cat1,'cats2':cat2, 'cats3':cat3, 'lat': lat, 'lng': lng,
                       'address': address})
    else:
        lat = 35.7053049802915
        lng = 51.3454769802915
        form = AdvertiseForm()
        formset = ImageFormSet(queryset=Image_Advertise.objects.none())
        return render(request, 'publishAd.html',
                      {'form': form, 'formset': formset, 'cats1': cat1,'cats2':cat2, 'cats3':cat3, 'lat': lat, 'lng': lng,
                       'address': ''})


def my_adv(request):
    if not request.user.is_authenticated:
        return redirect('index')

    user = request.user
    profile = Profile.objects.filter(user=user)
    customer = Customer.objects.filter(profile=profile[0])
    advertises = Advertise.objects.filter(seller=customer[0]).order_by('published_date')
    return render(request, 'my_ads.html', {'advertises': advertises})


def edit_profile(request):
    return redirect('index')


def logout(request):
    if request.user.is_authenticated:
        auth_logout(request)
    return redirect('index')


def view_ad(request, ad_id):
    advertise = Advertise.objects.get(pk=ad_id)
    loc = advertise.address
    # loc = Location_Advertise.objects.get(advertise=advertise)
    if request.method == 'POST':
        advertise.status = 'Has been sold'
        advertise.save()
    images = Image_Advertise.objects.filter(advertise=advertise)
    for im in images:
        urls = "Dar/"
    comments = Comment.objects.filter(advertise=advertise)
    urls = []
    for im in images:
        print(im.image.url)
    return render(request, 'view_ad.html',
                  {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat})


@login_required
def add_comment(request, ad_id):
    print("comment")
    advertise = Advertise.objects.get(pk=ad_id)
    images = Image_Advertise.objects.filter(advertise=advertise)
    comments = Comment.objects.filter(advertise=advertise)
    loc = Location_Advertise.objects.get(advertise=advertise)
    user = request.user
    user_form = SignupForm(instance=user)
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.advertise = advertise
                profile = Profile.objects.get(user=request.user)
                sender = Customer.objects.get(profile=profile)
                comment.sender = sender
                comment.time = datetime.now().date()
                comment.save()
    else:
        log = "تنها کاربران عضو سایت قادر به ثبت نظر هستند.\n برای ثبت نظر باید ابتدا وارد سایت شوید."
        return render(request, 'view_ad.html',
                      {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat, 'modalDisplay': 1, 'log':log})
    return render(request, 'view_ad.html',
                  {'advertise': advertise, "images": images, "comments": comments, "lng": loc.lng, "lat": loc.lat})


def recent_adv(request):
    number_of_item_to_show = 10
    advertises = Advertise.objects.order_by('published_date')[:number_of_item_to_show]
    return render(request, 'my_ads.html', {'advertises': advertises})


def save_credit(request, form):
    credit = form.save(commit=False)
    credit.time = datetime.now().date()
    profile = Profile.objects.get(user=request.user)
    seller = Customer.objects.get(profile=profile)
    credit.sender = seller
    credit.save()
    return credit



def ad_search(request):
     if request.method=='GET':
         return render(request, 'ad_search.html')
     if request.method=='POST':
         form=SearchForm(request.POST)
         if form.is_valid():
             title=form.cleaned_data['title']
             desc=form.cleaned_data['desc']
             min_price=form.cleaned_data['min_price']
             max_price=form.cleaned_data['max_price']
             if not min_price:
                 min_price=0
             if not max_price:
                 max_price=9999999999999999
             if not title:
                 title=""
             if not desc:
                 desc=""
             ads = Advertise.objects.filter(Q(title__icontains=title) | Q(desc__icontains=desc),
                                            Q(price__gte=min_price, price__lte=max_price)).order_by('priority')
             if ads:
                 return render(request, 'show_search_result.html', {'ads': ads})
             else:
                 return render(request, 'empty_search_result.html')






@login_required
def edit_profile(request):
    """
    This function is for editting manager profile. With this section, manager can get his previeus profile and change them.
    :param request: Contain manager id for getting its information and Updating them.
    :return: After entering information, if there is not any fault in filling field, return to main page of manager site.
    """

    user = request.user
    user_form = SignupForm(instance=user)
    log = ''
    if request.user.is_authenticated:
        if request.method == 'POST':
            user = request.user
            profile = Profile.objects.filter(user=user)
            customer = Customer.objects.filter(profile=profile[0])
            user_form = SignupForm(request.POST, instance=user)
            if user_form.is_valid():
                usernew = user_form.save()
                profile.user = usernew
                customer.profile = profile
                customer.phone_number = request.POST.get('phone_number')
                profile.save()
                customer.save()
                log = "اطلاعات شما با موفقیت ویرایش شد."
    return render(request, 'edit_profile.html', {'user': user, 'form': user_form, 'log': log})

def transaction_history(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    customer = Customer.objects.get(profile=profile)
    transactions = Transaction.objects.filter(customer=customer).order_by('-time')
    return render(request, "transaction_history.html", {"transactions": transactions})

def credits(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    customer = Customer.objects.get(profile=profile)
    reserve_crs = Reserve_credit.objects.filter(customer=customer,status="reserved")
    reserved_value = 0
    credit = customer.credit
    for rsrv_val in reserve_crs:
        reserved_value += rsrv_val.value
    return render(request, "credit_status.html", {"credit":credit, "reserved":reserved_value , "reserve_objects": reserve_crs})

def invite():

    return

