import os

from django.urls import path
from .views import *
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from Auction.views import make_auction, make_bid
from payment.views import charge_account
from notification.views import *

urlpatterns = [
                  url(r'^costumerBase/$', costumerHome, name='costumerHome'),
                  url(r'^add_advertisement/$', publish_adv, name='publish_ad_form'),
                  url(r'^my_advertisement/$', my_adv, name='my_adv'),
                  url(r'^edit_profile/$', edit_profile, name='edit_profile'),
                  url(r'^logout/$', logout, name='logout'),
                  url(r'^(?P<ad_id>[0-9]+)/$', view_ad, name='view_ad'),
                  url(r'^home/$', recent_adv, name='home'),
                  url(r'^(?P<ad_id>[0-9]+)/add_comment/$', add_comment, name='add_comment'),
                  url(r'^(?P<ad_id>[0-9]+)/add_auction/$', make_auction, name='add_auction'),
                  url(r'^(?P<ac_id>[0-9]+)/make_bid/$', make_bid, name='make_bid'),
                  url(r'^charge_account/$', charge_account, name='charge_account'),
                  url(r'^invite/$', invite, name='invite'),
                  url(r'^search/$', ad_search, name='customer_search'),
                  url(r'^transaction_history/$', transaction_history, name='transaction_history'),
                  url(r'^credits/$', credits, name='credits'),
                  url(r'^new_notification/$', new_notification, name='new_notification'),
                  url(r'^all_notification/$', all_notification, name='all_notification'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
