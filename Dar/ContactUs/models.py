from django.db import models
from secondHand.models import *
from django.utils import timezone
# Create your models here.

class Complaint(models.Model):
    customer = models.ForeignKey(Customer, related_name='complaint_customer', on_delete=models.CASCADE)
    message = models.CharField(max_length=500, default="Nothing", blank=False, null=False)
    time = models.DateTimeField(default=timezone.now())
    read = models.BooleanField(default=False)

