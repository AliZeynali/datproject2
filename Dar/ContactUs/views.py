from django.shortcuts import render
from .forms import ComplaintForm
from .models import Complaint
from secondHand.models import *
# Create your views here.

def contact(request):
    return render(request, "contact.html")


def complaint(request):
    try:
        if request.method == 'POST':
            form = ComplaintForm(request.POST)
            if form.is_valid():
                message = form.cleaned_data['message']
                user = request.user
                profile = Profile.objects.get(user=user)
                customer = Customer.objects.get(profile=profile)
                complaint = Complaint(customer=customer, message=message)
                complaint.save()
                return render(request, "complaint.html", {"result": "پیام شما با موفقیت ثبت شد"})
        else:
            return render(request, "complaint.html")
    except Exception:
        return render(request, "complaint.html", {"result": "سامانه ثبت شکایت هم اکنون در دسترس نمی‌باشد."})