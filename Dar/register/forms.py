from django import forms


class RegisterForm(forms.Form):
    fname = forms.CharField(max_length='100')
    lname = forms.CharField(max_length='100')
    email = forms.CharField(max_length='100')
    username = forms.CharField(max_length='100')
    password = forms.CharField(max_length='100')
    re_typed_password = forms.CharField(max_length='100')
    nationalID = forms.IntegerField()
    phone_number = forms.IntegerField()
    code = forms.CharField(required=False)
    location_address=forms.CharField(max_length='400')


