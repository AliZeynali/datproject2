from django.shortcuts import render
from .forms import RegisterForm
from django.contrib.auth import login as auth_login
from django.shortcuts import render, redirect
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from secondHand.models import *
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.http import HttpResponse
from django.contrib import messages
from django.utils.encoding import smart_str
from urllib.request import urlopen
import urllib
import json

def send_email(user, current_site, email):
    mail_subject = 'فعال کردن حساب کاربری شما در وبسایت در'
    message = render_to_string('acc_active_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        'token': account_activation_token.make_token(user),
    })
    email = EmailMessage(
        mail_subject, message, to=[email]
    )
    email.send()


def register(request):
    # if not request.user.is_authenticated:
    #  return redirect('index')
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            fname = form.cleaned_data['fname']
            lname = form.cleaned_data['lname']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            re_typed_password = form.cleaned_data['re_typed_password']
            nationalID = form.cleaned_data['nationalID']
            location_address = form.cleaned_data['location_address']
            phone_number = form.cleaned_data['phone_number']
            code = form.cleaned_data['code']
            if password != re_typed_password:
                log = "Passwords do not match"
                return render(request, 'register.html', {'log': log})
            user = User(username=username, first_name=fname, last_name=lname)
            user.email = email
            user.set_password(password)
            user.is_active = False
            user.save()
            profile = Profile(user=user, role='customer', nationalID=nationalID,
                              location_address=location_address)
            profile.save()
            customer = Customer(profile=profile, phone_number=phone_number, invitation_code=code)
            customer.save()
            current_site = get_current_site(request)
            send_email(user, current_site, email)
            messages.success(request, "حساب کاربری شما با موفقیت ساخته شد.")
            return render(request, 'Home.html', {'modalDisplay': 1})
        else:
            messages.warning(request, "رمزها با یکدیگر همخوانی ندارند.")

    return render(request, 'register.html')




def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        auth_login(request, user)
        # return redirect('home')
        return render(request, 'Home.html', {'modalDisplay': 2})
    else:
        return HttpResponse('Activation link is invalid!')