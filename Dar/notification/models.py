from django.db import models
from django.utils import timezone
from secondHand.models import *

class Notification(models.Model):
    customer = models.ForeignKey(Customer, related_name='customer', on_delete=models.CASCADE)
    message = models.CharField(max_length=500, default="Nothing", blank=False, null=False)
    time = models.DateTimeField(default=timezone.now())
    read = models.BooleanField(default=False)

    def get_message(self):
        if self.message == "Nothing":
            return "پیام خالی می‌باشد"
        return self.message
