from django.shortcuts import render
from .models import Notification
from secondHand.models import *
# Create your views here.

def new_notification(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    customer = Customer.objects.get(profile=profile)
    new_notifs = Notification.objects.filter(customer=customer, read=False).order_by('-time')
    for notif in new_notifs:
        notif.read = True
        notif.save()
    return render(request,"notification.html", {"notifs":new_notifs,"title":"پیام‌های جدید خوانده‌نشده"})



def all_notification(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    customer = Customer.objects.get(profile=profile)
    notifs = Notification.objects.filter(customer=customer).order_by('-time')
    return render(request, "notification.html", {"notifs": notifs , "title":"لیست‌ تمامی پیام‌های شما"})

